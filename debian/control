Source: libdnf
Section: libs
Priority: optional
Maintainer: RPM packaging team <team+pkg-rpm@tracker.debian.org>
Uploaders: Frédéric Pierret <frederic.pierret@qubes-os.org>,
           Luca Boccassi <bluca@debian.org>,
Build-Depends: debhelper-compat (= 13),
 dh-package-notes,
 dh-sequence-python3,
 python3-dev,
 libpython3-dev,
 cmake,
 pkgconf,
 libglib2.0-dev,
 libsolv-dev (>= 0.7.14-1.1~),
 libsolvext-dev (>= 0.7.14-1.1~),
 libsolv-tools (>= 0.7.14-1.1~),
 librpm-dev (>= 4.19.1.1~),
 libzstd-dev,
 librepo-dev (>= 1.18.1~),
 gtk-doc-tools,
 python3-sphinx,
 check,
 libgpgme-dev,
 libssl-dev,
 libjson-c-dev,
 libmodulemd-dev (>= 2.11.2~),
 libsmartcols-dev,
 libsqlite3-dev,
 gettext,
 swig,
 libcppunit-dev,
 doxygen,
 python3-breathe,
 rpm-common (>= 4.19.1.1~),
Rules-Requires-Root: no
Standards-Version: 4.7.0
Homepage: https://github.com/rpm-software-management/libdnf
Vcs-Browser: https://salsa.debian.org/pkg-rpm-team/libdnf
Vcs-Git: https://salsa.debian.org/pkg-rpm-team/libdnf.git
#Testsuite: autopkgtest-pkg-python

Package: libdnf-dev
Section: libdevel
Architecture: any
Multi-Arch: same
Depends: ${misc:Depends},
 libdnf2t64 (= ${binary:Version}),
 libsolv-dev (>= 0.7.14-1.1~),
 libsolvext-dev (>= 0.7.14-1.1~),
Breaks: gir1.2-libdnf-1.0 (<< ${binary:Version}),
        libdnf2t64 (<< 0.73.1-1~), libdnf2 (<< 0.73.1-1~),
Replaces: libdnf2t64 (<< 0.73.1-1~), libdnf2 (<< 0.73.1-1~),
Description: libdnf - development files
 A library providing simplified C and Python APIs to libsolv.
 .
 This package contains the development header files for the
 libdnf library.

Package: libdnf2t64
Provides: libdnf, ${t64:Provides}
Architecture: any
Multi-Arch: same
Depends: ${shlibs:Depends}, ${misc:Depends},
 libdnf2-common (= ${source:Version}),
Replaces: libdnf2,
Breaks: libdnf2 (<< ${source:Version}),
Description: libdnf - shared library
 A library providing simplified C and Python APIs to libsolv.
 .
 This package provides the libdnf shared library.

Package: libdnf2-common
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends},
Description: libdnf - common files
 A library providing simplified C and Python APIs to libsolv.
 .
 This package provides common files for the libdnf library.

Package: libdnf-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${misc:Depends},
Description: Documentation for the libdnf library (common documentation)
 A library providing simplified C and Python APIs to libsolv.
 .
 This package installs common documentation for the libdnf
 C bindings.

Package: python3-hawkey
Architecture: any
Multi-Arch: no
Section: python
Depends: ${shlibs:Depends}, ${python3:Depends}, ${misc:Depends},
 libdnf2t64 (= ${binary:Version}),
 python3-libdnf (= ${binary:Version}),
Provides: ${python3:Provides},
Suggests: python3-hawkey-docs
Description: Python bindings for the hawkey library (Python 3)
 A library providing simplified C and Python APIs to libsolv.
 .
 This package installs the hawkey library for Python 3.

Package: python3-hawkey-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: ${sphinxdoc:Depends}, ${misc:Depends},
         libjs-jquery, libjs-underscore,
Description: Python 3 bindings for the hawkey library (common documentation)
 A library providing simplified C and Python APIs to libsolv.
 .
 This package installs common documentation for the hawkey Python
 3 bindings.

Package: python3-libdnf
Architecture: any
Multi-Arch: no
Section: python
Depends: ${shlibs:Depends}, ${python3:Depends}, ${misc:Depends},
 libdnf2t64 (= ${binary:Version}),
Provides: ${python3:Provides},
Description: Python bindings for the libdnf library (Python 3)
 A library providing simplified C and Python APIs to libsolv.
 .
 This package installs the libdnf library for Python 3.
